<?php

namespace EdgeLabs\RoutingBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class RoutingExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * Allow an extension to prepend the extension configurations.
     *
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        // get all bundles
        $bundles = $container->getParameter('kernel.bundles');

        if (isset($bundles['CmfRoutingBundle'])) {
            $config = array(
                'chain'   => array(
                    'routers_by_id' => array(
                        'router.default'             => 200,
                        'cmf_routing.dynamic_router' => 100
                    )
                ),
                'dynamic' => array(
                    'enabled'                => true,
                    'route_collection_limit' => 20,
                    'generic_controller'     => 'edgelabs.route.page_controller:indexAction',
                    'persistence'            => array('orm' => array('enabled' => true))
                )
            );

            $container->prependExtensionConfig('cmf_routing', $config);

        } else {
            throw new RuntimeException("In order to have EdgeLabs\\RoutingBundle running, we need symfony-cmf/routing-bundle, which appears to be missing!");
        }
    }

}
