<?php

namespace EdgeLabs\RoutingBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\NonUniqueResultException;
use EdgeLabs\RoutingBundle\Exception\ExistingRouteException;
use EdgeLabs\RoutingBundle\Model\AutoRouteInterface;
use Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route;

/**
 * Class AutoRouteListener (based on Symfony\Cmf\Bundle\RoutingAutoBundle\Doctrine\Phpcr\AutoRoute)
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\RoutingBundle\EventListener
 */
class AutoRouteListener
{

    /**
     * @param OnFlushEventArgs $args
     * @throws ExistingRouteException
     * @throws \Doctrine\ORM\ORMException
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        /** @var $em EntityManager */
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        $updates = $uow->getScheduledEntityUpdates();
        foreach ($updates as $entity) {
            if ($entity instanceof AutoRouteInterface) {
                $route = $this->process($entity, $em);
                $em->persist($route);
                $uow->computeChangeSets();
            }
        }
    }

    /**
     * @param PreFlushEventArgs $args
     * @throws ExistingRouteException
     * @throws \Doctrine\ORM\ORMException
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        /** @var $em EntityManager */
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        $inserts = $uow->getScheduledEntityInsertions();
        foreach ($inserts as $entity) {
            if ($entity instanceof AutoRouteInterface) {
                $route = $this->process($entity, $em);
                $em->persist($route);
            }
        }

        $updates = $uow->getScheduledEntityUpdates();
        foreach ($updates as $entity) {
            if ($entity instanceof AutoRouteInterface) {
                $route = $this->process($entity, $em);
                $em->persist($route);
            }
        }
    }

    /**
     * @param $entity AutoRouteInterface
     * @param $em     EntityManager
     *
     * @return \Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route
     *
     * @throws ExistingRouteException
     */
    protected function process(&$entity, $em)
    {
        $route = $entity->getRoute();
        $routeName = $entity->getRouteKey() . '_' . $entity->getUUID();

        if (!$route) {
            $route = new Route();
            $route->setName($routeName); // set route routeName only on initial insert to avoid relationship problems.
        }

        $route->setStaticPrefix($entity->getURI());
        $route->setDefault('_uuid', $entity->getUUID());
        $route->setDefault('_content', $entity);
        $route->setOption('utf8', true);
        $route->setContent($entity);

        // Prevent duplicating URIs.
        try {
            $routeCheck = $this->findRoute($em, $entity);
            if ($routeCheck != null && $routeCheck->getName() !== $routeName) {
                throw new ExistingRouteException;
            }
        } catch (\Exception $e) {
            throw new ExistingRouteException(null, null, $e);
        }

        $entity->setRoute($route);

        return $route;
    }

    /**
     * @param EntityManager $em
     * @param AutoRouteInterface $entity
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    private function findRoute($em, $entity)
    {
        $qb = $em->createQueryBuilder()
                 ->select('r')
                 ->from(\Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route::class, 'r')
                 ->where('r.name = :uuid or r.staticPrefix = :prefix')
                 ->setParameter('uuid', $entity->getRouteKey() . '_' . $entity->getUUID())
                 ->setParameter('prefix', $entity->getURI());

        return $qb->getQuery()->getOneOrNullResult();
    }
}
