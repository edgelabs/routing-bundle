<?php

namespace EdgeLabs\RoutingBundle\Exception;

/**
 * Class ExistingRouteException
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\RoutingBundle\Exception
 */
class ExistingRouteException extends \Exception
{
    protected $message = 'An object with this uri already exists!';
}
