<?php

namespace EdgeLabs\RoutingBundle\Controller;

use EdgeLabs\RoutingBundle\TestBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @author Steve Todorov <steve.todorov@carlspring.com>
 */
class PageController extends AbstractController
{
    /**
     * @param Page $contentDocument
     *
     * @return array
     */
    public function indexAction($contentDocument)
    {
        if ($contentDocument->getVisible() != true) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            $this->getParameter('cms_routing.page_controller_template'),
            array(
                'content' => $contentDocument,
                'title' => $contentDocument->getTitle(),
                'body' => $contentDocument->getBody(),
            )
        );
    }
}
