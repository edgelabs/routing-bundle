<?php

namespace EdgeLabs\RoutingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RoutingBundle extends Bundle
{
    public function getParent()
    {
        return 'CmfRoutingBundle';
    }
}
