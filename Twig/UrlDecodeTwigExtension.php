<?php

namespace EdgeLabs\RoutingBundle\Twig;

/**
 * Class TwigExtension
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\RoutingBundle\Twig
 */
class UrlDecodeTwigExtension extends \Twig_Extension
{

    /**
     * @return array|\Twig\TwigFunction[]
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('url_decode', function ($name) {
                try {
                    return urldecode($name);
                } catch (\Exception $exception) {
                    return null;
                }
            })
        );
    }

    /**
     * @return array|\Twig\TwigFilter[]
     */
    public function getFilters()
    {
        return array(
            new \Twig\TwigFilter('url_decode', function($name) {
                try {
                    return urldecode($name);
                } catch (\Exception $exception) {
                    return null;
                }
            })
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'edgelabs.routing.twig.url_decode.extension';
    }
}
