<?php

namespace EdgeLabs\Tests\RoutingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EdgeLabs\RoutingBundle\Model\BaseRoute;

/**
 * Class Page
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\Tests\RoutingBundle\Entity
 *
 * @ORM\Table(name="pages")
 * @ORM\Entity(repositoryClass="EdgeLabs\Tests\RoutingBundle\Entity\PageRepository")
 */
class Page extends BaseRoute implements \Serializable
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="normalized_url", type="string", length=255)
     */
    private $normalizedTitleUrl;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get $this->normalizedTitleUrl
     *
     * @return mixed
     */
    public function getNormalizedTitleUrl()
    {
        return $this->normalizedTitleUrl;
    }

    /**
     * Set $this->normalizedTitleUrl
     *
     * @param mixed $normalizedTitleUrl
     */
    public function setNormalizedTitleUrl($normalizedTitleUrl)
    {
        $this->normalizedTitleUrl = $normalizedTitleUrl;
    }

    public function getURI()
    {
        return '/' . $this->getLocale() . '/' . ltrim($this->getNormalizedTitleUrl(), '/');
    }

    public function getRouteKey()
    {
        return 'page';
    }
}
