<?php

namespace EdgeLabs\Tests\RoutingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use EdgeLabs\Test\RoutingBundle\Entity\Page;
use Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route;

/**
 * Class PageRepository
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\Tests\RoutingBundle\Entity
 */
class PageRepository extends EntityRepository
{
    public function findAllWithRoute($locale = 'en', $column = 'id', $order = 'ASC')
    {
        $query = $this->createQueryBuilder('page')
                      ->addSelect('route')
                      ->leftJoin('page.route', 'route')
                      ->where('page.locale = :locale');

        $query->setParameter('locale', $locale);

        return $query->getQuery();
    }

    public function countPages()
    {
        $qb = $this->getEntityManager()
                   ->createQueryBuilder()
                   ->select('COUNT(page.id)')
                   ->from(Page::class, 'page');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $uuid
     *
     * @return null|Route
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findRouteForUUID($uuid)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
                   ->select('r')
                   ->from(Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route::class, 'r')
                   ->where('r.defaults LIKE :uuid')
                   ->setParameter('uuid', '%' . $uuid . '%');;

        return $qb->getQuery()->getOneOrNullResult();
    }
}

?>
