<?php

namespace EdgeLabs\Tests\RoutingBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class PageFrontendController
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\Tests\RoutingBundle\Controller
 */
class PageController extends Controller
{
    /**
     * @Template("TestBundle::index.html.twig")
     */
    public function indexAction($contentDocument)
    {
        if ($contentDocument->getVisible() != true) {
            throw $this->createNotFoundException();
        }

        return array(
            'page' => $contentDocument,
        );
    }
}
