<?php

namespace EdgeLabs\Tests\RoutingBundle\TestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EdgeLabs\Tests\RoutingBundle\Entity\Page;

/**
 * Class LoadPage
 *
 * @author Steve Todorov <steve.todorov@carlspring.com>
 */
class LoadPage implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {
        $page1 = new Page();
        $page1->setTitle('Testing visible page title');
        $page1->setSeo("seo keyword");
        $page1->setBody('Testing page content.');
        $page1->setLocale('en');
        $page1->setNormalizedTitleUrl('testing-visible-page');
        $page1->setVisible(true);
        $manager->persist($page1);

        $page2 = new Page();
        $page2->setTitle('Testing invisible page title');
        $page2->setSeo("seo keyword");
        $page2->setBody('Testing page content.');
        $page2->setLocale('en');
        $page2->setNormalizedTitleUrl('testing-invisible-page');
        $page2->setVisible(false);
        $manager->persist($page2);

        $manager->flush();
    }

}

