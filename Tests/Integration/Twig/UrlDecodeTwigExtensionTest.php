<?php

namespace EdgeLabs\Tests\RoutingBundle\Tests\Integration\Twig;

use PHPUnit\Framework\TestCase;


/**
 * Author: Steve Todorov <steve.todorov@carlspring.com>
 */
class UrlDecodeTwigExtensionTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testFilter($encodedString, $decodedString)
    {
        $extension = new \EdgeLabs\RoutingBundle\Twig\UrlDecodeTwigExtension();
        $callable = $extension->getFilters()[0]->getCallable();
        $output = $callable($encodedString);
        $this->assertEquals($output, $decodedString);
    }

    public static function provider()
    {
        return [
            ['/bg/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B8', '/bg/Документи'],
            ['/bg/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%BD%D0%B8%D0%BA', '/bg/Правилник']
        ];
    }
}
