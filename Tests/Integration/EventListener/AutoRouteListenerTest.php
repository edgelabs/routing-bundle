<?php

namespace EdgeLabs\Tests\RoutingBundle\Tests\Integration\EventListener;

use EdgeLabs\RoutingBundle\Exception\ExistingRouteException;
use EdgeLabs\Tests\RoutingBundle\Entity\Page;
use EdgeLabs\Tests\RoutingBundle\TestBundle\DataFixtures\ORM\LoadPage;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AutoRouteListenerTest
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\RoutingBundle\Tests\EventListener
 */
class AutoRouteListenerTest extends WebTestCase
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ContainerInterface
     */
    protected $clientContainer;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->clientContainer = $this->client->getContainer();

        $this->loadFixtures(array(LoadPage::class));
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    private function getManager()
    {
        return $this->clientContainer->get('doctrine')->getManager();
    }

    public function testAutoRoute()
    {
        $em = $this->getManager();

        $repo = $em->getRepository(Page::class);
        $pages = $repo->findAllWithRoute()->getResult();

        $this->assertCount(2, $pages, "Expected 2 pages in database, found " . count($pages));

        /**
         * @var Page $page
         */
        foreach ($pages as $page) {
            $this->assertEquals($page->getRoute()->getStaticPrefix(), $page->getURI());
            $this->assertEquals($page->getSeo(), "seo keyword");
            $this->assertEquals($page->getLocale(), "en");
        }
    }

    /**
     * @depends testAutoRoute
     */
    public function testAutoRouteDelete()
    {
        $em = $this->getManager();

        $repo = $em->getRepository(Page::class);
        $pages = $repo->findAllWithRoute()->getResult();
        $routeId = $pages[0]->getRoute()->getId();

        $em->remove($pages[0]);
        $em->flush();

        $pages = $repo->findAllWithRoute()->getResult();

        $this->assertCount(1, $pages, "Expected 1 pages in database, found " . count($pages));

        $route = $em->getRepository(Route::class)->find($routeId);

        $this->assertNull($route, "Expected route " . $routeId . " to have been deleted, but it's still in the database!");

    }

    /**
     * @depends testAutoRoute
     */
    public function testAutoRouteUpdate()
    {
        $em = $this->getManager();

        $repo = $em->getRepository(Page::class);
        $pages = $repo->findAllWithRoute()->getResult();
        /**
         * @var $page Page
         */
        $page = $pages[0];

        $content = 'testing new body.';
        $newUri = str_replace(' ', '-', $content);

        $page->setBody($content);
        $page->setNormalizedTitleUrl($newUri);

        $em->persist($page);
        $em->flush();

        $pages = $repo->findAllWithRoute()->getResult();
        $page = $pages[0];
        $route = $page->getRoute();

        $this->assertEquals($page->getBody(), $route->getContent()->getBody(), "Route content does not match page!");
        $this->assertEquals($page->getRoute()->getStaticPrefix(), '/' . $page->getLocale() . '/' . $newUri, "Route static uri hasn't changed!");
    }

    /**
     * @depends testAutoRoute
     */
    public function testAutoRouteDuplicateExceptionOnNewRecord()
    {
        $this->expectException(ExistingRouteException::class);

        $em = $this->getManager();

        $duplicate = new Page();
        $duplicate->setTitle('Testing visible page title');
        $duplicate->setBody('Testing page content.');
        $duplicate->setLocale('en');
        $duplicate->setNormalizedTitleUrl('testing-visible-page');
        $duplicate->setVisible(true);

        $em->persist($duplicate);
        $em->flush();
    }

    /**
     * @depends testAutoRoute
     */
    public function testAutoRouteDuplicateExceptionOnUpdateRecord()
    {
        $this->expectException(ExistingRouteException::class);

        $em = $this->getManager();

        $repo = $em->getRepository(Page::class);
        $pages = $repo->findAllWithRoute()->getResult();
        /**
         * @var $page Page
         */
        $page = $pages[0];

        // Try to update the url to match an existing one.
        // This should throw an ExistingRouteException.
        $page->setNormalizedTitleUrl('testing-invisible-page');

        $em->persist($page);
        $em->flush();
    }

}
