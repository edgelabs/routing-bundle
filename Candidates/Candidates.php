<?php

namespace EdgeLabs\RoutingBundle\Candidates;

use Symfony\Component\HttpFoundation\Request;

/**
 * Author: Steve Todorov <steve.todorov@carlspring.com>
 *
 * @see https://github.com/symfony-cmf/routing/issues/183
 */
class Candidates extends \Symfony\Cmf\Component\Routing\Candidates\Candidates
{
    public function getCandidates(Request $request)
    {
        $url = urldecode($request->getPathInfo());
        $candidates = $this->getCandidatesFor($url);

        $locale = $this->determineLocale($url);
        if ($locale) {
            $candidates = array_unique(array_merge($candidates, $this->getCandidatesFor(substr($url, strlen($locale) + 1))));
        }

        return $candidates;
    }
}
