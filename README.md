RoutingBundle
===

# Installation

To install run the following command in composer.

```
# composer require edgelabs/routing-bundle`
```

Afterwards, please enable the bundle in `AppKernel.php`

```
    $bundles = array(
        new Symfony\Cmf\Bundle\RoutingBundle\CmfRoutingBundle(),
        new EdgeLabs\RoutingBundle\RoutingBundle()
    );
```


# Configuration

There is no need to configure anything as this bundle is based on `CMFRoutingBundle` 
and most of the configuration is already set by the `RoutingBundle`. 
In case you need to change anything, this is how the configuration should look:

```
cmf_routing:
    chain:
        routers_by_id:
            router.default: 200
            cmf_routing.dynamic_router: 100
    dynamic:
        enabled: true
        route_collection_limit: 20
        persistence:
            orm:
                enabled: true
```

This will tell symfony to try matching the routes using the default router. If the default router does not contain the route it will continue trying with the dynamic router.
Using this approach makes the application a bit more reliable - i.e. if your database is temporarily offline, your site will still be able to partly function.

# Usage

Once you've installed this bundle you only need to create an `Entity` which extends `BaseRoute`. Checkout the `TestBundle` inside the `Tests` directory to see a working example.
Then you need to add `controllers_by_class` to `cmf_routing` so that a controller handles the requests for your entity:

```
cmf_routing:
    dynamic:
        controllers_by_class:
            App\MyBundle\Entity\Page: App\MyBundle\PageFrontendController::indexAction
```

An example of a controller and viewer are present in the `Tests` directory as well.