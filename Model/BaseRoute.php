<?php

namespace EdgeLabs\RoutingBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route;

/**
 * Class BaseRoute
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\RoutingBundle\Model
 */
abstract class BaseRoute implements AutoRouteInterface, \Serializable
{

    /**
     * @var
     * @ORM\Column(name="uuid", type="string", length=40)
     */
    protected $uuid;

    /**
     * @var \Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route
     *
     * @ORM\OneToOne(targetEntity="Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route", cascade={"persist"}, orphanRemoval=true)
     */
    protected $route;

    /**
     * @var
     */
    protected $routes;

    /**
     * @var
     *
     * @ORM\Column(name="locale", type="string", length=10)
     */
    protected $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_keywords", type="text", nullable=true)
     */
    protected $seo;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    protected $body;

    protected $visible = true;

    public function __construct()
    {
        $this->routes = new ArrayCollection();
        $this->uuid = $this->generateUUID();
    }

    /**
     * Normalize and santizie string to become url-friently.
     *
     * @param $url
     *
     * @return mixed
     */
    public function normalizeURI($url)
    {
        $string = $url;
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);

        return $string;
    }

    /**
     * @param \Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route $route
     *
     * @return mixed|void
     */
    public function setRoute(Route $route)
    {
        $this->route = $route;
    }

    /**
     * @return \Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * This should return a unique string.
     *
     * @return string
     */
    public function getUUID()
    {
        return $this->uuid;
    }

    /**
     * Returns the full URI which will be inserted as a route path.
     * If this method is not extended in the implementation, the route will be the UUID of the entity.
     *
     * @return string
     */
    public function getURI()
    {
        return $this->getUUID();
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this;
    }

    /**
     * Get the route key.
     *
     * This key will be used as route name instead of the symfony core compatible
     * route name and can contain any characters.
     *
     * Return null if you want to use the default key.
     *
     * @return string the route name
     */
    abstract function getRouteKey();

    /**
     * Get the routes that point to this content.
     *
     * @return ArrayCollection|\Symfony\Component\Routing\Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Generates the UUID.
     */
    protected function generateUUID()
    {
        $string = substr(str_shuffle(MD5(microtime(true))), 0, 6) . '.';
        return uniqid($string);
    }

    /**
     * Get $this->locale
     *
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set $this->locale
     *
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get $this->seo
     *
     * @return string
     */
    public function getSeo()
    {
        return $this->seo;
    }

    /**
     * Set $this->seo
     *
     * @param string $seo
     */
    public function setSeo($seo)
    {
        $this->seo = $seo;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get $this->body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set $this->body
     *
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get $this->visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set $this->visible
     *
     * @param boolean $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    public function serialize()
    {
        return serialize(array(
            'uuid' => $this->uuid,
            'seo' => $this->seo,
            'locale' => $this->locale,
            'title' => $this->title,
            'body' => $this->body,
            'visible' => $this->visible
        ));
    }

    public function unserialize($serialized)
    {
        $data = @unserialize($serialized);
        $this->uuid = $data['uuid'];
        $this->title = $data['title'];
        $this->body = $data['body'];
        $this->visible = $data['visible'];

        if (array_key_exists('locale', $data)) {
            $this->locale = $data['locale'];
        }

        if (array_key_exists('seo', $data)) {
            $this->seo = $data['seo'];
        }
    }
}
