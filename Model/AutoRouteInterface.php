<?php

namespace EdgeLabs\RoutingBundle\Model;

use Symfony\Cmf\Bundle\RoutingBundle\Doctrine\Orm\Route;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Cmf\Component\Routing\RouteReferrersReadInterface;

/**
 * Interface AutoRouteInterface
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\RoutingBundle\Model
 */
interface AutoRouteInterface extends RouteObjectInterface, RouteReferrersReadInterface
{

    /**
     * This should return a unique string.
     *
     * @return string
     */
    public function getUUID();

    /**
     * Returns the full URI which will be inserted as a route path
     *
     * @return string
     */
    public function getURI();

    /**
     * Normalizes a string to become url-friendly
     *
     * @param $string
     *
     * @return mixed
     */
    public function normalizeURI($string);

    /**
     * @return mixed
     */
    public function getRoute();

    /**
     * @param Route $route
     *
     * @return mixed
     */
    public function setRoute(Route $route);

    public function setLocale($locale);

    public function getLocale();

    public function getSeo();

    public function setSeo($keywords);

    public function getTitle();

    public function setTitle($title);

    public function getBody();

    public function setBody($body);

    public function getVisible();

    public function setVisible($bool);

}
